import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'megaVuex',
      component: () => import(/* webpackChunkName: "megaVuex" */ './views/MegaVuex.vue')
    },
    {
      path: '/normal-vuex',
      name: 'normalVuex',
      component: () => import(/* webpackChunkName: "normalVuex" */ './views/NormalVuex.vue')
    },
    {
      path: '/no-vuex',
      name: 'noVuex',
      component: () => import(/* webpackChunkName: "noVuex" */ './views/NoVuex.vue')
    },
    {
      path: '/should-i',
      name: 'shouldI',
      component: () => import(/* webpackChunkName: "noVuex" */ './views/ShouldI.vue')
    }
  ]
})
