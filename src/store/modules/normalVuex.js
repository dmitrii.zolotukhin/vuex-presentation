import { getUsers } from '../../api'
import { SET_USERS } from '../mutationTypes'

export const normalVuex = {
  namespaced: true,

  state: {
    users: []
  },

  getters: {
    users: state => state.users
  },

  actions: {
    async pullUsers ({ commit }) {
      try {
        const users = await getUsers()
        commit(SET_USERS, users)
      } catch (e) {
        throw e
      }
    }
  },

  mutations: {
    [SET_USERS]: (state, users) => { state.users = users }
  }

}
