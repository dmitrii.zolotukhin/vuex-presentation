import { getUsers } from '../../api'
import { MEGA_MUTATE_ACTION_RETURN_USERS, MEGA_MUTATE_ASYNC_RETURN_USERS } from '../mutationTypes'

export const megaVuex = {
  namespaced: true,
  actions: {
    async actionWhichReturnResponse () {
      const users = await getUsers()
      return users
    },
    async actionWhichReturnAsyncCommit ({ commit }) {
      return commit
    }
  },

  mutations: {
    async [MEGA_MUTATE_ACTION_RETURN_USERS] (state, callback) {
      state.users = await this.dispatch('megaVuex/actionWhichReturnResponse')
      callback(state.users)
    },
    async [MEGA_MUTATE_ASYNC_RETURN_USERS] (state, callback) {
      const users = await getUsers()
      callback(users)
    }
  }
}
