export const MEGA_MUTATE_ACTION_RETURN_USERS = 'MEGA_MUTATE_ACTION_RETURN_USERS ' + String.fromCodePoint(0x1F4A9)
export const MEGA_MUTATE_ASYNC_RETURN_USERS = 'MEGA_MUTATE_ASYNC_RETURN_USERS ' + String.fromCodePoint(0x1F4A9)

export const SET_USERS = 'Setting fresh users from api ' +
  String.fromCodePoint(0x1F525) +
  String.fromCodePoint(0x1F608) +
  String.fromCodePoint(0x1F525)
