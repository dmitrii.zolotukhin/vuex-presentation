import Vue from 'vue'
import Vuex from 'vuex'
import { megaVuex } from './modules/megaVuex'
import { normalVuex } from './modules/normalVuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  namespaced: true,
  modules: {
    megaVuex,
    normalVuex
  }
})
