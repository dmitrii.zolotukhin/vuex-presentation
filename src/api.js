import axios from 'axios'

const url = 'https://jsonplaceholder.typicode.com/'
const stripData = response => response.data

export const getUsers = async () => {
  return stripData(await axios.get(url + 'users'))
}
