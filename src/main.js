import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import './registerServiceWorker'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import eventBus from './eventBus'

Vue.use(Vuetify)
Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.config.errorHandler = function (err) {
  eventBus.$emit('error', err)
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
